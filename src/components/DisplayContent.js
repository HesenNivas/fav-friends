import React, { useState } from 'react';
import './DisplayContent.css';
import DisplayField from './DisplayField';

const DisplayContent = ({ dataStore, handleDataStoreDelete, handleDataStoreFavorite }) => {
	const [currentPage, setCurrentPage] = useState(1);
	const contentPerPage = 4;
	const totalPages =
		dataStore.length % contentPerPage === 0
			? Number(dataStore.length / contentPerPage)
			: Number((dataStore.length / contentPerPage).toString().split('.')[0]) + 1;
	const toDisplay = (data) => {
		data = data.slice((currentPage - 1) * 4, currentPage * 4);
		return data;
	};

	const handlePreviousClick = () => {
		if (currentPage > 1) setCurrentPage(currentPage - 1);
	};

	const handleNextClick = () => {
		if (currentPage < totalPages) setCurrentPage(currentPage + 1);
	};

	return (
		<div className="display-container m-3">
			{dataStore &&
				toDisplay(dataStore).map((data) => {
					return (
						<DisplayField
							data={data}
							handleDataStoreDelete={handleDataStoreDelete}
							handleDataStoreFavorite={handleDataStoreFavorite}
							key={data.id}
						></DisplayField>
					);
				})}
			<div className="d-flex justify-content-end">
				<nav>
					<ul className="pagination">
						{currentPage !== 1 && (
							<li className="page-item" onClick={handlePreviousClick}>
								<span className="page-link" href="#">
									Previous
								</span>
							</li>
						)}
						<li className="page-item">
							<span className="page-link" href="#">
								{`${currentPage}/${totalPages}`}
							</span>
						</li>
						{currentPage !== totalPages && (
							<li className="page-item" onClick={handleNextClick}>
								<span className="page-link" href="#">
									Next
								</span>
							</li>
						)}
					</ul>
				</nav>
			</div>
		</div>
	);
};

export default DisplayContent;
