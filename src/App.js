import './App.css';
import { useState } from 'react';
import DisplayContent from './components/DisplayContent';

function App() {
	const [dataStore, setDataStore] = useState([
		{ id: 1, name: 'Akash Singh', isFavorite: false },
		{ id: 2, name: 'Shivangi Sharma', isFavorite: false },
		{ id: 3, name: 'Rahul Gupta', isFavorite: false },
	]);
	const [displayData, setDisplayData] = useState([...dataStore]);
	const [inputData, setInputData] = useState('');
	const [isSearched, setIsSearched] = useState(false);
	const [isSortByFavorite, setIsSortByFavorite] = useState(false);
	const [isAddError, setIsAddError] = useState(false);

	const handleAddFriend = () => {
		setIsAddError(false);
		if (dataStore.filter((data) => data.name.toLowerCase() === inputData.toLowerCase()).length === 0) {
			dataStore.push({ id: dataStore.length + 1, name: inputData, isFavorite: false });
			setInputData('');
		} else {
			setIsAddError(true);
		}
		setDisplayData([...dataStore]);
		setIsSearched(false);
	};

	const handleSearch = () => {
		setIsAddError(false);
		setDisplayData([...dataStore.filter((d) => d.name.toLowerCase().includes(inputData.toLowerCase()))]);
		setIsSearched(true);
	};

	const resetSearch = () => {
		setDisplayData([...dataStore]);
		setInputData('');
		setIsSearched(false);
	};

	const handleDataStoreDelete = (id) => {
		let updatedDS = dataStore.filter((data) => data.id !== id);
		setDataStore(updatedDS);
		setDisplayData(updatedDS);
		setInputData('');
		setIsSearched(false);
	};

	const handleUnfavoriteAll = () => {
		let updatedDS = dataStore.map((data) => {
			data.isFavorite = false;
			return data;
		});
		setDataStore(updatedDS);
		setDisplayData(updatedDS);
		setInputData('');
		setIsSearched(false);
	};

	const handleDataStoreFavorite = (id) => {
		setDataStore(
			dataStore.map((data) => {
				if (data.id === id) data.isFavorite = !data.isFavorite;
				return data;
			})
		);
	};

	const reorderDataStore = (ds) => {
		ds = [...ds.sort((a, b) => b.id - a.id)];
		if (isSortByFavorite) {
			return [...ds.filter((d) => d.isFavorite), ...ds.filter((d) => !d.isFavorite)];
		}
		return [...ds];
	};

	return (
		<div className="container d-flex justify-content-center">
			<main className="main-container">
				<h3 className="main-title p-4">Friends List</h3>
				<div className="m-3 main-content d-flex align-items-start">
					<div className="d-flex flex-column" style={{ width: '100%' }}>
						<input
							className={isAddError ? 'form-control is-invalid' : 'form-control'}
							type="text"
							name=""
							id=""
							placeholder="Enter your friend's name"
							value={inputData}
							onChange={(e) => setInputData(e.target.value)}
							onKeyDown={(e) => e.key === 'Enter' && handleAddFriend()}
						/>
						<div className="invalid-feedback">Friend already present in the list.</div>
					</div>

					<button className="main-add-button btn btn-large btn-success ms-3" onClick={handleAddFriend}>
						Add
					</button>
					{!isSearched ? (
						<button className="main-search-button btn btn-large btn-success ms-3" onClick={handleSearch}>
							Search
						</button>
					) : (
						<div className="p-2 ms-3" onClick={resetSearch}>
							<svg
								xmlns="http://www.w3.org/2000/svg"
								width="20"
								height="20"
								fill="currentColor"
								className="bi bi-x-lg mx-3"
								viewBox="0 0 16 16"
							>
								<path d="M1.293 1.293a1 1 0 0 1 1.414 0L8 6.586l5.293-5.293a1 1 0 1 1 1.414 1.414L9.414 8l5.293 5.293a1 1 0 0 1-1.414 1.414L8 9.414l-5.293 5.293a1 1 0 0 1-1.414-1.414L6.586 8 1.293 2.707a1 1 0 0 1 0-1.414z" />
							</svg>
						</div>
					)}
				</div>
				{displayData.length !== 0 ? (
					<div>
						<div className="d-flex justify-content-end me-3">
							<button
								className={
									isSortByFavorite ? 'btn btn-primary btn-sm' : 'btn btn-outline-primary btn-sm'
								}
								onClick={() => setIsSortByFavorite(!isSortByFavorite)}
							>
								Sort by Favorite
							</button>
							<button
								className="btn btn-outline-primary btn-sm ms-3"
								onClick={() => handleUnfavoriteAll()}
							>
								Unfavorite All
							</button>
						</div>
						<DisplayContent
							dataStore={reorderDataStore(displayData)}
							handleDataStoreDelete={handleDataStoreDelete}
							handleDataStoreFavorite={handleDataStoreFavorite}
						></DisplayContent>
					</div>
				) : (
					<div className="d-flex justify-content-center my-5">
						<h4>{!isSearched ? `No Friend Found` : `Searched Friend not found`}</h4>
					</div>
				)}
			</main>
		</div>
	);
}

export default App;
